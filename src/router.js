import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import UserInList from './views/UserInList.vue'
import UserInEdit from './views/UserInEdit.vue'
import AdEdit from './views/AdEdit.vue'
import AdList from './views/AdList.vue'
import Money from './views/Money.vue'
import UserMoneyList from './views/UserMoneyList.vue'
import UserMoneyEdit from './views/UserMoneyEdit.vue'
import Login from './views/Login.vue'
import Floor from './views/Floor.vue'
import Cell from './views/Cell.vue'
import Type from './views/Type.vue'

Vue.use(Router)

// 防止多次点击路由报错
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const router =  new Router({
  routes: [{
      path: '/',
      name: 'home',
      component: Home,
      children: [{
          path: '/',
          component: UserInList
        },
        //业主管理
        {
          path: '/userin/list',
          name: 'UserInList',
          component: UserInList
        },
        {
          path: '/userin/edit',
          name: 'UserInEdit',
          component: UserInEdit
        },
        {
          path: '/userin/edit/:id',
          component: UserInEdit,
          props: true
        },

        //管理员管理
        {
          path: '/admin/list',
          name: 'AdList',
          component: AdList
        },
        {
          path: '/admin/edit',
          name: 'AdEdit',
          component: AdEdit
        },
        {
          path: '/admin/edit/:id',
          component: AdEdit,
          props: true
        },
        //金额管理
        {
          path: '/money',
          name: 'Money',
          component: Money,
        },
        {
          path: '/user/money/list',
          name: 'UserMoneyList',
          component: UserMoneyList,
        },
        {
          path: '/user/money/edit',
          name: 'UserMoneyEdit',
          component: UserMoneyEdit,
        },
        //其余管理
        {
          path: '/admin/floor',
          name: 'Floor',
          component: Floor
        },
        {
          path: '/admin/cell',
          name: 'Cell',
          component: Cell
        },
        {
          path: '/admin/type',
          name: 'Type',
          component: Type
        }
      ]
    },
    //登陆管理
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        isPublic: true,
      }
    }
  ]
})

//导航守卫
router.beforeEach((to, from, next) => {
  if (!to.meta.isPublic && !sessionStorage.token) {
    return next('/login')
  }
  next()
})

export default router