module.exports = {
    //输出的文件夹路径
    outputDir: __dirname + '/../server/public/admin',
    // 配置编译生成的斜杠是'/admin'，否则就是'/'
    publicPath: process.env.NODE_ENV === 'production'
        ? '/admin/'
        : '/'
}
